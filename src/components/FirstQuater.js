import React,{useState} from "react";
import { FormGroup, Label, Input,Form, Button, Col, Row,CardBody} from "reactstrap";
import { Control, Errors, LocalForm, actions } from 'react-redux-form';



const FirstQuater = ({handleSubmit, players}) => {
  const [selectedId, setSelectedId] = useState({
    p1: null,
    p2: null,
    p3: null,
    p4: null,
    p5: null
  });
    return (
   <CardBody>
     <LocalForm
        onSubmit={(values) => handleSubmit(values)}
        className="form-horizontal"
        model="team"
            > 
        <FormGroup>
        <Row md={6}>
        <Col md={6}>
          <Control
                model=".player1"
                onChange={(e)=>setSelectedId({...selectedId, p1: e.target.value})}
                component={props =>  <Input {...props} type="select" name="select" id="position1" >
                  <option value="">Select Player</option>
                {players.playerList.map((p)=><option value={p.id}>{p.first_name}{p.lastname}</option>)}
                  </Input>}
               />
          </Col>
          < Col md={6}>
            <Control
                model=".positionPlayer1"
                component={props =>  <Input {...props} type="select" name="select" id="positionPlayer1">
                <option value="">Select Position</option>
                { players.playerList.find(row => row.id == selectedId.p1) &&  players.playerList.find(row => row.id == selectedId.p1).positions.map((position)=><option value={position.id}>{position.name}</option>)}
                  </Input>}
             />
          </Col>
          </Row>
          </FormGroup>
         <FormGroup>
          <Row md={6}>
          <Col md={6}>
               <Control
                model=".player2"
                onChange={(e)=>setSelectedId({...selectedId, p2: e.target.value})}
                component={props =>  <Input {...props} type="select" name="select" id="position2" >
                <option value="">Select Player</option>
                {players.playerList.map((p, index)=><option value={p.id}>{p.first_name}{p.lastname}</option>)}
                  </Input>}
               />
          </Col>
          < Col md={6}>
          <Control
                model=".positionPlayer2"
                component={props =>  <Input {...props} type="select" name="select" id="positionPlayer2">
               <option value="">Select Position</option>
               { players.playerList.find(row => row.id == selectedId.p2) && players.playerList.find(row => row.id == selectedId.p2).positions. map((position)=><option value={position.id}>{position.name}</option>)}
                  </Input>}
             />
          </Col>
          </Row>
          </FormGroup>
          <FormGroup>
          <Row md={6}>
          <Col md={6}>
             <Control
                model=".player3"
                onChange={(e)=>setSelectedId({...selectedId, p3: e.target.value})}
                component={props =>  <Input {...props} type="select" name="select" id="position3" >
                <option value="">Select Player</option>
                {players.playerList.map((p, index)=><option value={p.id}>{p.first_name}{p.lastname}</option>)}
                  </Input>}
               />
          </Col>
          < Col md={6}>
          <Control
                model=".positionPlayer3"
                component={props =>  <Input {...props} type="select" name="select" id="positionPlayer3">
                <option value="">Select Position</option>
                {players.playerList.find(row => row.id == selectedId.p3)&& players.playerList.find(row => row.id == selectedId.p3).positions.map((position)=><option value={position.id}>{position.name}</option>)}
                  </Input>}
             />
          </Col>
          </Row>
          </FormGroup>
          <FormGroup>
          <Row md={6}>
          <Col md={6}>
              <Control
                model=".player4"
                onChange={(e)=>setSelectedId({...selectedId, p4: e.target.value})}
                component={props =>  <Input {...props} type="select" name="select" id="position4" >
                <option value="">Select Player</option>
                {players.playerList.map((p, index)=><option value={p.id}>{p.first_name}{p.lastname}</option>)}
                  </Input>}
               />
              </Col>
              < Col md={6}>
              <Control
                model=".positionPlayer4"
                component={props =>  <Input {...props} type="select" name="select" id="positionPlayer4">
                <option value="">Select Position</option>
                {players.playerList.find(row => row.id == selectedId.p4) && players.playerList.find(row => row.id == selectedId.p4).positions. map((position)=><option value={position.id}>{position.name}</option>)}
                  </Input>}
             />
              </Col>
              </Row>
          </FormGroup>
          <FormGroup>
          <Row md={6}>
          <Col md={6}>
             <Control
                model=".player5"
                onChange={(e)=>setSelectedId({...selectedId, p5: e.target.value})}
                component={props =>  <Input {...props} type="select" name="select" id="position5" >
                <option value="">Select Player</option>
                {players.playerList.map((p)=><option value={p.id}>{p.first_name}{p.lastname}</option>)}
                  </Input>}
               />
              </Col>
              < Col md={6}>
              <Control
                model=".positionPlayer5"
                component={props =>  <Input {...props} type="select" name="select" id="positionPlayer5">
                <option value="">Select Position</option>
                {players.playerList.find(row => row.id == selectedId.p5) && players.playerList.find(row => row.id == selectedId.p5).positions.map((position)=><option value={position.id}>{position.name}</option>)}
                  </Input>}
             />
              </Col>
              </Row>
          </FormGroup>
      <Row md={6}>
      <Col md={9 }></Col>
      <Col md={1}>
      <Button type="submit" color="primary">Save</Button>
      </Col>
      </Row>
      
      </LocalForm>
      </CardBody>
     
  );
}

export default FirstQuater;