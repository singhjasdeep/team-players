import React,{useState} from "react";
import { Row, Col } from "reactstrap";
import { FormGroup, Label, Input,CardBody, Button} from "reactstrap";
import { Control, Errors, LocalForm, actions } from 'react-redux-form';
import PropTypes from 'prop-types';
import { Multiselect } from 'multiselect-react-dropdown';
import { playerPosition } from '../utilities/constent'


const ComposeTeam = ({handleSubmit}) => {
  const [selectedValue, setSelectedValue]= useState([]);
  const onSelect = (selectedList, selectedItem) => {
    setSelectedValue(selectedList)
}
 
const onRemove = (selectedList, removedItem) => {
  setSelectedValue(selectedList)
} 
   return (
      <CardBody>
       <LocalForm
        onSubmit={(values) => handleSubmit(values, selectedValue)}
        className="form-horizontal"
        model="player"
            > 
          <FormGroup>
            <Label for="firstname">First Name</Label>
              <Control
                model=".first_name"
                component={props =>  <Input {...props} type="text" name="fristname" id="firtsname" placeholder="First Name" required />}
               />
            </FormGroup>
          <FormGroup>
            <Label for="lastname">Last Name</Label>
              <Control
                  model=".last_name"
                  component={props => <Input {...props} type="text" id="lastname" name="lastname" placeholder="Last Name" required />}
                />
          </FormGroup>
          <FormGroup>
            <Label for="lastname">Height</Label>
              <Control
                model=".height"
                component={props => <Input {...props} type="number" name="height" id="height" placeholder="Height" step=".1" required/>}
              />
          </FormGroup>
          <FormGroup>
            <Label for="playerposition">Position</Label>
            <Control
                model=".position"
                component={props => <Multiselect
                  {...props}
                  options={playerPosition} 
                  selectedValues={selectedValue} 
                  onSelect={onSelect} 
                  onRemove={onRemove}
                  displayValue="name"
                />}
              />
            
          </FormGroup>
        <Row md={6}>
        <Col md={9 }></Col>
        <Col md={1}>
          <Button type="submit" color="primary">Save</Button>
        </Col>
        </Row>
      </LocalForm>
    </CardBody>
     
  );
}

ComposeTeam.propTypes = {
    handleSubmit:PropTypes.func.isRequired
};


export default ComposeTeam;