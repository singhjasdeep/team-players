import { ADD_PLAYER, ADD_TEAM } from "../../actions/action_constant";
const initialState = {
  playerList: [],
  team:[]
};
export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_PLAYER:
      return {...state, playerList: [...state.playerList,{id:Date.now() , ...action.data}]};
      case ADD_TEAM:
      return {...state, team: [...state.team,{id:Date.now() , ...action.data}]};
    default:
      return state;
  }
};

