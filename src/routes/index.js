import React from "react";
import { Switch } from "react-router-dom";
import AppRoute from "./AppRoute";
import Layout from "../components/Layout";
import player from "../containers/Players";
export default function Routes() {
  return (
    <Switch>
      <AppRoute exact path="/" component={player} layout={Layout} />
    </Switch>
  );
}
