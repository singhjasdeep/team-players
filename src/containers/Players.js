import React, { useState } from "react";
import { Row, Col ,Button} from "reactstrap";
import {
  Card,
  CardTitle
} from 'reactstrap';
import ComposeTeam from '../components/ComposeTeam'; 
import FirstQuater from '../components/FirstQuater';
import { useDispatch, useSelector } from "react-redux";
import { addPlayer, addTeam } from "../actions/players";

export default () => {
  const dispatch = useDispatch();
  const [tab,setTab] = useState(1);
  const { players } = useSelector(state => state);

  const handleSubmit = (value, selectedValue) =>{
    if(tab === 1){
      dispatch(addPlayer({...value, positions:selectedValue}))
      setTab(2);
    }  else {
      dispatch(addTeam({...value}))
    }    
   }
 

  return (
    <Row>
      <Col md={3}></Col>
      <Col md={6}>
      <Card>
          <CardTitle >
          <Button color="primary" onClick={()=>setTab(1)}>Compose Team</Button>{' '}
          <Button color="secondary" onClick={()=>setTab(2)}>First Quater</Button>{' '}</CardTitle>
          <hr/>
          {tab == 1 ? <ComposeTeam handleSubmit={handleSubmit} /> : <FirstQuater handleSubmit={handleSubmit} players={players}/>}
          </Card>
        </Col>      
      </Row>
  );
}
