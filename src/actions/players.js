import { ADD_PLAYER, ADD_TEAM } from "./action_constant";

/********** Add Player**********/
export const addPlayer = data => ({
  data,
  type: ADD_PLAYER
});
/********** Add Team**********/
export const addTeam = data => ({
  data,
  type: ADD_TEAM
});

