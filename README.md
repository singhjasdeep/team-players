## React-Redux team-player demo

### Provides
- react ^16.13.0
- redux ^7.2.0
- redux-thunk ^2.3.0

### Installation

- `yarn install`
- `yarn start`
- `yarn test`
- `yarn build`

## Application will run on 
```- http://localhost:3000